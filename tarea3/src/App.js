import React, { useState } from 'react';
import './App.css';
import { Button, Form, Modal, Input, Divider } from 'antd';

var arrLista = [];

function App() {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [lista, setLista] = useState([]);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const onFinish = (values) => {
    arrLista.push(values.texto);
    setLista(arrLista);
    setIsModalVisible(false);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className="App">
      <h1>TAREA 03</h1>
      <Divider />
      {lista.map((value, index) =>{
        console.log("valor: ", value);
        return <h3 key={index}>{value}</h3>
      })}
      <Divider />
      <Button type="primary" onClick={showModal}>Agregar a lista</Button>
      <Modal title="Agregar a lista" visible={isModalVisible} footer={null}>
        <Form
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Texto"
            name="texto"
            rules={[{ required: true, message: 'Por favor ingrese el texto' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Agregar
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default App;
