import { Component } from 'react';
import styled, { createGlobalStyle } from 'styled-components';

import Formulario from './component/Formulario';
import ListaCitas from './component/ListaCitas';

const GlobalStyle = createGlobalStyle`
html{
  height: 100%;
}

*,*::after,::before{
  margin: 0;
  padding: 0;
  box-sizing: inherit
}

body{
  font-family: Arial, Helvetica, sans-serif;
  height: 100%;
  margin: 0;
  color: #555;
  box-sizing: border-box
}
`;

const Container = styled.div`
  max-width: 1140px;
  background-color: #f6f6f6;
  margin: 0 auto;
`;

class App extends Component {
  state = {
    listaCitas: [],
    editarCitas: [],
  };

  agregarCita = (cita) => {
    console.log('la cita es:');
    console.log(cita);
    let listaTemporal = { listaCitas: [...this.state.listaCitas, cita] };
    this.setState(listaTemporal);

    // localStorage.setItem('lista', JSON.stringify(listaTemporal));
  };

  eliminar = (id) => {
    const listaActualizada = this.state.listaCitas.filter(
      (cita) => cita.id !== id
    );
    this.setState({ listaCitas: listaActualizada });
  };

  actualizar = (id) => {
    const listaActualizada = this.state.listaCitas.filter(
      (cita) => cita.id === id
    );
    console.log("listaActualizada", listaActualizada);
    this.setState({ editarCitas: listaActualizada });
  };

  // componentDidMount() {
  //   console.log('ejecutando');
  //   let listaRecuperada = JSON.parse(localStorage.getItem('lista'));
  //   console.log(listaRecuperada);
  //   this.setState(listaRecuperada);
  // }

  render() {
    return (
      <div className='App'>
        <GlobalStyle />
        <Container>
          <Formulario agregarCita={this.agregarCita} editarCita={this.state.editarCitas} />
          <ListaCitas listaCitas={this.state.listaCitas} eliminar={this.eliminar} actualizar={this.actualizar}/>
        </Container>
      </div>
    );
  }
}

export default App;
