import React from 'react';
import styled from 'styled-components';

const Card = styled.section`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  flex-direction: column;
  max-width: 18em;
  border-radius: 5px;
  overflow: hidden;
  border: 1px solid #eee;
  margin: 1em;
`;
const CardHeader = styled.div`
  background-color: #ffffb0;
  width: 100%;
  padding: 0.5em;
  border-bottom: 1px solid #e2e2e2;
`;

const CardBody = styled.div`
  background-color: #ffffb0;
  width: 100%;
  padding: 0.5em;
`;

const ListItems = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`;

const Item = styled.li`
  border-top: 1px solid #cbcbcb;
  :first-of-type {
    border-top: none;
  }
  padding: 0.5em;
`;

const CardFooter = styled.div`
  background-color: #ffffffb0;
  width: 100%;
  padding: 0.5em;
  display: flex;
  justify-content: space-between;
`;

const Button = styled.button`
  display: block;
  color: #fff;
  font-size: 0.9rem;
  border: 0px;
  border-radius: 5px;
  height: 40px;
  padding: 0 9px;
  cursor: pointer;
  box-sizing: border-box;
  background-color: ${(props) =>
    props.color === 'primary' ? '#ff6e6e' : '#5555ff'};
`;

const Cita = ({ info, eliminar, actualizar }) => {
  const { nombre, apellido, direccion, fecha, hora, sintomas } = info;
  return (
    <Card>
      <CardHeader>
        <strong>Descripción de cita:</strong>
      </CardHeader>
      <CardBody>
        <ListItems>
          <Item>
            <strong>Nombre:</strong> {nombre}
          </Item>
          <Item>
            <strong>Apellido:</strong> {apellido}
          </Item>
          <Item>
            <strong>Dirección:</strong> {direccion}
          </Item>
          <Item>
            <strong>Fecha:</strong> {fecha}
          </Item>
          <Item>
            <strong>Hora:</strong> {hora}
          </Item>
          <Item>
            <strong>Sintomas:</strong> {sintomas}
          </Item>
        </ListItems>
      </CardBody>
      <CardFooter>
        <Button color='primary' onClick={eliminar}>
          Eliminar
        </Button>
        <Button onClick={actualizar}>Actualizar</Button>
      </CardFooter>
    </Card>
  );
};

export default Cita;
