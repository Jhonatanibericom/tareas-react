import React, { Component } from 'react';
import styled from 'styled-components';
import Titulo from '../Titulo';
import Cita from '../Cita';

const ListaContainer = styled.section`
  display: flex;
  flex-wrap: wrap;
  padding: 2em;
`;

class ListaCitas extends Component {
  render() {
    const { listaCitas, eliminar, actualizar } = this.props;
    return (
      <div>
        <Titulo titulo='Lista de Citas'></Titulo>

        <ListaContainer>
          {listaCitas.length > 0 ? (
            listaCitas.map((cita) => (
              <Cita key={cita.id} info={cita} eliminar={() => eliminar(cita.id)} actualizar={() => actualizar(cita.id)} />
            ))
          ) : (
            <p style={{ textAlign: 'center', width: '100%' }}>
              No tiene citas agendadas
            </p>
          )}
        </ListaContainer>
      </div>
    );
  }
}

export default ListaCitas;
