import styled, { css } from 'styled-components';

export const Form = styled.form`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 1em 2em;
`;

export const FormGroup = styled.div`
  flex: 0 0 ${(props) => (props.col === '1' ? 100 : 100 / props.col - 2)}%;
  @media (max-width: 600px) {
    flex: 0 0 100%;
  }
`;

const sharedInputStyled = css`
  display: block;
  width: 100%;
  background-color: #eee;
  border-radius: 5px;
  border: 1px solid #ddd;
  margin: 10px 0 20px 0;
  padding: 20px;
  box-sizing: border-box;
`;

export const StyleInput = styled.input`
  height: 40px;
  ${sharedInputStyled}
`;

export const StyleArea = styled.textarea`
  height: 5em;
  resize: none;
  ${sharedInputStyled}
`;

export const Button = styled.button`
  display: block;
  background-color: #f7797d;
  color: #fff;
  font-size: 0.9rem;
  border: 0px;
  border-radius: 5px;
  height: 40px;
  padding: 0 20px;
  cursor: pointer;
  box-sizing: border-box;
`;
