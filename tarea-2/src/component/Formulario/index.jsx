import React, { Component } from 'react';
import { Form, FormGroup, StyleInput, StyleArea, Button } from './style';
import Titulo from '../Titulo';

import { v4 as uuidv4 } from 'uuid';

const initialState = {
  cita: {
    nombre: '',
    apellido: '',
    direccion: '',
    fecha: '',
    hora: '',
    sintomas: '',
  },
};

class Formulario extends Component {
  constructor(props) {
    super(props);
    this.state = {  ...initialState };
  }
  

  handleChange = (e) => {
    this.setState({
      cita: { ...this.state.cita, [e.target.name]: e.target.value },
    });
  };

  validaDatos = () => {
    const { cita } = this.state;
    let valorDeRetorno = true;

    if (
      cita.nombre === '' ||
      cita.apellido === '' ||
      cita.direccion === '' ||
      cita.fecha === '' ||
      cita.hora === '' ||
      cita.sintomas === ''
    ) {
      valorDeRetorno = false;
    }

    return valorDeRetorno;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.validaDatos()) {
      const nuevaCita = { ...this.state.cita };
      nuevaCita.id = uuidv4();

      this.props.agregarCita(nuevaCita);
      this.setState({ ...initialState });
    } else {
      alert('Los campos son requeridos');
    }
  };

  componentDidUpdate(previousProps, previousState) {
    // Typical usage (don't forget to compare props):
    let editarCita = this.props.editarCita;
    console.log('editar cita', editarCita);
    if (editarCita[0] != undefined) {
      alert(JSON.stringify(editarCita[0]));
    //   console.log('entra');
    //   this.setState({ 
    //     cita: {
    //       nombre: 'aaa',
    //       apellido: 'aaa',
    //       direccion: 'aaasss',
    //       fecha: '',
    //       hora: '',
    //       sintomas: 'sdsfsd',
    //     },
    //   })
    }
  }

  render() {
    return (
      <div>
        <Titulo titulo='Formulario de Citas'></Titulo>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup col='2'>
            <label>Nombre</label>
            <StyleInput
              type='text'
              name='nombre'
              placeholder='Ingresar Nombre'
              onChange={this.handleChange}
              value={this.state.cita.nombre}
            />
          </FormGroup>
          <FormGroup col='2'>
            <label>Apellido</label>
            <StyleInput
              type='text'
              name='apellido'
              placeholder='Ingresar Apellido'
              onChange={this.handleChange}
              value={this.state.cita.apellido}
            />
          </FormGroup>
          <FormGroup col='1'>
            <label>Dirección</label>
            <StyleInput
              type='text'
              name='direccion'
              placeholder='Ingresar Dirección'
              value={this.state.cita.direccion}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup col='3'>
            <label>Fecha de Cita</label>
            <StyleInput
              type='date'
              name='fecha'
              onChange={this.handleChange}
              value={this.state.cita.fecha}
            />
          </FormGroup>
          <FormGroup col='3'>
            <label>Hora</label>
            <StyleInput
              type='time'
              name='hora'
              onChange={this.handleChange}
              value={this.state.cita.hora}
            />
          </FormGroup>
          <FormGroup col='3'>
            <label>Sintomas</label>
            <StyleArea
              type='text'
              name='sintomas'
              placeholder='Ingresar sintomas'
              onChange={this.handleChange}
              value={this.state.cita.sintomas}
            />
          </FormGroup>
          <FormGroup col='1'>
            <Button type='submit'>Reservar</Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

export default Formulario;
