import React from 'react';
import styled from 'styled-components';

const Title = styled.h1`
  color: #7271fb;
  text-align: center;
  margin: 0;
`;

const Titulo = ({titulo}) => {
  return <Title>{titulo}</Title>;
};

export default Titulo;
