import React from 'react';
import Estudiante from './Estudiante';

const Principal = () => {
  const sumaNotas = (notas) => notas.reduce((a, i) => a + i);
  return (
    <div>
      <Estudiante
        titulo={<h1>Estudiante</h1>}
        notas={[10, 16, 11]}
        nombre='Juan'
        suma={sumaNotas}
      />
    </div>
  );
};

export default Principal;
