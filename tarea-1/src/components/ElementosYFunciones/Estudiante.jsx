import React from 'react';

const Estudiante = ({ titulo, notas, nombre, suma }) => {
  const promedio = suma(notas) / notas.length;

  return (
    <div>
      {titulo}
      <p>
        mi nombre es: <strong>{nombre}</strong>
      </p>
      <p>
        mis notas son: <strong>{notas.join(', ')}</strong>
      </p>
      <p>
        suma de notas: <strong>{suma(notas)}</strong>
      </p>
      <p>
        mis promedio es: <strong>{promedio}</strong>
      </p>
    </div>
  );
};

export default Estudiante;
