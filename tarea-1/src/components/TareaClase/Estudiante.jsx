import React, { Component } from 'react';

class Estudiante extends Component {
    render() {
        console.log(this.props);
        const { nombre, notas, titulo, suma } = this.props;
        const promedio = suma(notas) / notas.length;
        return (
            <div>
                {titulo}
                <p>
                    mi nombre es: <strong>{nombre}</strong>
                </p>
                <p>
                    mis notas son: <strong>{notas.join(', ')}</strong>
                </p>
                <p>
                    suma de notas: <strong>{suma(notas)}</strong>
                </p>
                <p>
                    mis promedio es: <strong>{promedio}</strong>
                </p>
            </div>
        );
    }
}

export default Estudiante;