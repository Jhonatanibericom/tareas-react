import React, { Component } from 'react';
import Estudiante from './Estudiante';

class Principal extends Component {
    render() {
        const sumaNotas = (notas) => notas.reduce((a, i) => a + i);
        return (
            <div>
                <Estudiante
                    titulo={<h1>Estudiante Clase</h1>}
                    notas={[10, 16, 11]}
                    nombre='Juan Perez'
                    suma={sumaNotas}
                />
            </div>
        );
    }
}

export default Principal;