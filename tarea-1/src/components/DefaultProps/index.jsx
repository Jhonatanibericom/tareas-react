import React from 'react';
import Soldado from './Soldado';

const Principal = () => {
  return (
    <div>
      <Soldado
        fullName={{ nombre: 'Eduardo', apellido: 'Lulichac' }}
        edad={30}
        mayorDeEdad={'false'}
      />
    </div>
  );
};

export default Principal;
