import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Soldado extends Component {
  render() {
    const { fullName, edad, mayorDeEdad } = this.props;
    return (
      <div>
        <p>
          Nombre Completo: {fullName.nombre} {fullName.apellido}
        </p>
        <p>edad: {edad}</p>
        <p>
          Usted:{' '}
          {mayorDeEdad ? 'esta listo para servir' : 'no esta listo para servir'}
        </p>
      </div>
    );
  }
}

Soldado.propTypes = {
  fullName: PropTypes.object,
  edad: PropTypes.number,
  mayorDeEdad: PropTypes.bool,
};

// Soldado.defaultProps =  {
//     fullName: {nombre: 'Nombre', apellido: 'Apellido'},
//     edad: 0,
//     mayorDeEdad: true
// }

export default Soldado;
