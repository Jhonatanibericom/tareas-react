import React from 'react';

const Persona = ({ nombre, edad }) => (
  <div>
    <p>
      Mi nombre es: <strong>{nombre}</strong> y tengo <strong>{edad}</strong>{' '}
      años
    </p>
  </div>
);

export default Persona;
