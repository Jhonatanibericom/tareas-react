import React from 'react';
import Persona from './Persona';

const Principal = () => {
  return (
    <div>
      <Persona nombre='Juan' edad={25}/>
      <Persona nombre='Maria' edad={20}/>
      <Persona nombre='Jose' edad={67}/>
    </div>
  );
};

export default Principal;
