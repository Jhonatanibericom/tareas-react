import React, {Component} from 'react';

class MiClase extends Component {
    render() {
        return (
            <div>
                <h2>Titulo del componente clase</h2>
                <p>parrafo del componente clase</p>
            </div>
        );
    }
}

export default MiClase;