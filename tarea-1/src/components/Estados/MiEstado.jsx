import React, { Component } from 'react';
import Propagacion from './Propagacion';

class MiEstado extends Component {
  //   state = {
  //     contador: 6,
  //     titulo: 'Hola Mundoo',
  //   };

  constructor(props) {
    super(props);
    this.state = {
      contador: this.props.numero,
      titulo: 'Hola desde el contructor',
      edad: 20,
      hoy: new Date(),
      cumpleanos: new Date('December 17, 1995 03:24:00'),
    };

    setInterval(() => {
      this.setState({
        ...this.state,
        contador: this.state.contador + 1,
      });
      //   this.setState((x) => ({
      //     ...x,
      //     contador: x.contador + 1,
      //   }));
    }, 1000);
  }

  render() {
    const { contador, titulo, edad, hoy, cumpleanos } = this.state;

    return (
      <div>
        <h1>Hola desde el componente estado</h1>
        <h3>{titulo}</h3>
        <p>la edad es: {edad}</p>
        <Propagacion contador={contador} />
        <p>hoy: {hoy.toString()}</p>
        <p>cumpleaños: {cumpleanos.toString()}</p>
      </div>
    );
  }
}

export default MiEstado;
