import React from 'react';

const FuncionModerna = () => {
  return (
    <div>
      <h1>Soy el titulo de la Funcion Moderna</h1>
      <p>Soy un parrafo de la funcion moderna</p>
    </div>
  );
};

export default FuncionModerna;