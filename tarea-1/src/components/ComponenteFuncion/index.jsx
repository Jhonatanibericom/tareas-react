
import FuncionModerna from './FuncionModerna';
import FuncionTradicional from './FuncionTradicional';

const Principal = () => (
    <div>
        <FuncionModerna/>
        <FuncionTradicional/>
    </div>
)

export default Principal;