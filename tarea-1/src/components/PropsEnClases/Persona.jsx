import React, { Component } from 'react';

class Persona extends Component {
  render() {
    const { nombre, edad } = this.props;
    return (
      <div>
        <p>
          Mi nombre es: <strong>{nombre}</strong> y tengo{' '}
          <strong>{edad}</strong> años
        </p>
      </div>
    );
  }
}

export default Persona;
