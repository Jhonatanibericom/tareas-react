import React from 'react';
import Persona from './Persona';

const Principal = () => {
  return (
    <div>
      <Persona nombre="Marco" edad={29}/>
      <Persona nombre="Antony" edad={25}/>
      <Persona nombre="Carlos" edad={20}/>
    </div>
  );
};

export default Principal;
