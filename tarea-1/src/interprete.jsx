import React from 'react';

// function App() {
//   return React.createElement(
//     "div",
//     {className: "App"},
//     React.createElement("h1", null, "Hola Mundo XD"),
//     React.createElement("p", null, "Soy un parrafo XD")
//   );
// }

function App() {
  return (
    <div className="App">
      <h1>Hola Mundo</h1>
      <p>Soy un parrafo</p>
    </div>
  );
}

export default App;

// function App() {
//   return (
//     <div className="App">
//       <h1>Hola Mundo</h1>
//       <p>Soy un parrafo</p>
//     </div>
//   );
// }