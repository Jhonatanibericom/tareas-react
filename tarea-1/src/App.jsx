// import ComponenteFuncion from './components/ComponenteFuncion';
// import ComponenteClase from './components/ComponenteClase'
// import PropsEnFunciones from './components/PropsEnFunciones';
// import PropsEnClases from './components/PropsEnClases';
// import ElementosYFunciones from './components/ElementosYFunciones';
// import DefaultProps from './components/DefaultProps';
// import Estados from './components/Estados';
import ComponentePropsClase from './components/TareaClase';
function App() {
  return (
    <div className='App'>
      {/* <ComponenteFuncion />
      <ComponenteClase /> */}
      {/* <p>----Props en Funciones -----</p>
      <PropsEnFunciones />
      <p>----Props en Clases -----</p>
      <PropsEnClases /> */}
      {/* <ElementosYFunciones /> */}
      {/* <DefaultProps /> */}
      {/* <Estados /> */}
      <ComponentePropsClase />
    </div>
  );
}

export default App;
