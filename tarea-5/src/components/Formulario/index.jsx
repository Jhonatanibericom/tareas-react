import React, { isValidElement, useEffect, useState } from 'react';
import EditarProducto from '../../views/productos/Editar';

const initDataForm = {
  nombre: '',
  precio: '',
  categoria: '',
  decripcion: '',
};

const errorsInit = {
  ...initDataForm,
};

const Formulario = ({
  editar = false,
  producto,
  agregarProductoNuevo,
  editarProducto,
}) => {
  const [dataForm, setDataForm] = useState(initDataForm);
  const [errors, setErrors] = useState(errorsInit);

  useEffect(() => {
    if (editar) {
      console.log('editando');
      console.log(producto);
      setDataForm({ ...producto });
    }
  }, []);

  const handleChange = (e) => {
    setDataForm({ ...dataForm, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: '' });
  };

  const isValid = () => {
    const localErrors = { ...errorsInit };
    let respuesta = true;

    for (let key in dataForm) {
      if (key !== 'id') {
        if (dataForm[key].trim() == '' || dataForm[key].length === 0) {
          localErrors[key] = 'campo requerido';
          respuesta = false;
        }
      }
    }
    setErrors({ ...localErrors });
    return respuesta;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isValid()) {
      if (!editar) {
        agregarProductoNuevo(dataForm);
      } else {
        editarProducto(dataForm);
      }
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className='form-row row'>
        <div className='form-group col-md-6'>
          <label htmlFor='inputEmail4'>Nombre</label>
          <input
            type='text'
            className='form-control'
            name='nombre'
            value={dataForm.nombre}
            onChange={handleChange}
          />
          <span style={{ color: 'red', fontSize: '14px' }}>
            {errors.nombre}
          </span>
        </div>
        <div className='form-group col-md-6'>
          <label htmlFor='inputPassword4'>Precio</label>
          <input
            type='text'
            className='form-control'
            name='precio'
            value={dataForm.precio}
            onChange={handleChange}
          />
          <span style={{ color: 'red', fontSize: '14px' }}>
            {errors.precio}
          </span>
        </div>
      </div>
      <div className='form-group d-flex justify-content-around mt-2'>
        <div className='form-check'>
          <input
            className='form-check-input'
            type='radio'
            name='categoria'
            value='Bebidas'
            onChange={handleChange}
            checked={dataForm.categoria === 'Bebidas'}
          />

          <label className='form-check-label' htmlFor='Bebidas'>
            Bebidas
          </label>
        </div>
        <div className='form-check'>
          <input
            className='form-check-input'
            type='radio'
            name='categoria'
            value='Cortes'
            onChange={handleChange}
            checked={dataForm.categoria === 'Cortes'}
          />
          <label className='form-check-label' htmlFor='Cortes'>
            Cortes
          </label>
        </div>
        <div className='form-check'>
          <input
            className='form-check-input'
            type='radio'
            name='categoria'
            value='Entradas'
            onChange={handleChange}
            checked={dataForm.categoria === 'Entradas'}
          />
          <label className='form-check-label' htmlFor='Entradas'>
            Entradas
          </label>
        </div>
        <div className='form-check'>
          <input
            className='form-check-input'
            type='radio'
            name='categoria'
            value='Postres'
            onChange={handleChange}
            checked={dataForm.categoria === 'Postres'}
          />
          <label className='form-check-label' htmlFor='Postres'>
            Postres
          </label>
        </div>
      </div>
      <span style={{ color: 'red', fontSize: '14px' }}>{errors.categoria}</span>
      <div className='text-center'></div>
      <div className='form-group'>
        <label htmlFor='descripcion'>Descripción</label>
        <textarea
          className='form-control'
          name='decripcion'
          id='exampleFormControlTextarea1'
          rows='3'
          value={dataForm.decripcion}
          onChange={handleChange}
        ></textarea>
        <span style={{ color: 'red', fontSize: '14px' }}>
          {errors.decripcion}
        </span>
      </div>
      <button type='submit' className='btn btn-primary mt-4'>
        {editar ? 'Editar Producto' : 'Agregar Producto'}
      </button>
    </form>
  );
};

export default Formulario;
