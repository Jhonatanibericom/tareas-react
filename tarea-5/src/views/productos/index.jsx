import React, { useContext, useEffect } from 'react';
import Tabla from '../../components/Tabla';

import { obtenerProductos } from '../../redux/actions/productos';
import { useDispatch, useSelector } from 'react-redux';

const Productos = () => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);

  const { listaProductos, deleteOk } = state.productos;

  useEffect(() => {
    dispatch(obtenerProductos());
  }, []);

  useEffect(() => {
    if (deleteOk) {
      dispatch(obtenerProductos());
    }
  }, [deleteOk]);

  return (
    <div>
      <div>
        <h1>Productos</h1>
        {listaProductos.length > 0 && <Tabla listaProductos={listaProductos} />}
      </div>
    </div>
  );
};

export default Productos;
