import React, { useContext, useEffect } from 'react';
import Formulario from '../../components/Formulario';

import { agregarProducto } from '../../redux/actions/productos';
import { useDispatch, useSelector } from 'react-redux';

const AgregarProducto = (props) => {
  // const productoContext = useContext(ProductoContext);

  // const { addOk, agregarProducto } = productoContext;
  const dispatch = useDispatch();
  const state = useSelector((state) => state);

  const { addOk } = state.productos;

  useEffect(() => {
    if (addOk) {
      props.history.push('/productos');
    }
  }, [addOk]);

  const nuevoProducto = (producto) => {
    dispatch(agregarProducto(producto));
  };

  return (
    <div>
      <h1>Agregar Producto</h1>
      <Formulario agregarProductoNuevo={nuevoProducto} />
    </div>
  );
};

export default AgregarProducto;
