import React, { useContext, useEffect } from 'react';
import Formulario from '../../components/Formulario';

// import ProductoContext from '../../context/Productos/ProductoContext';
import { obtenerProducto, editarProducto } from '../../redux/actions/productos';
import { useDispatch, useSelector } from 'react-redux';

const EditarProducto = (props) => {
  const dispatch = useDispatch();

  const state = useSelector((state) => state);

  const { editOk, producto } = state.productos;

  // const { editOk, producto, obtenerProducto, editarProducto } = productoContext;

  useEffect(() => {
    console.log('mira el id');
    console.log(props.match.params.id);
    dispatch(obtenerProducto(props.match.params.id));
  }, []);

  useEffect(() => {
    if (editOk) {
      props.history.push('/productos');
    }
  }, [editOk]);

  const tieneProducto = Object.keys(producto).length;

  const edicion = (producto) => {
    dispatch(editarProducto(producto));
  };

  return (
    <div>
      <h1>Editar Producto</h1>
      {tieneProducto > 0 && (
        <Formulario
          producto={producto}
          editar={true}
          editarProducto={edicion}
        />
      )}
    </div>
  );
};

export default EditarProducto;
