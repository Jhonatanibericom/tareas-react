const reducer = (state, action) => {
  switch (action.type) {
    case 'LLENAR_PRODUCTOS':
      return {
        listaProductos: action.payload,
        producto: {},
        addOk: false,
        editOk: false,
        deleteOk: false,
      };
    case 'AGREGAR_PRODUCTO':
      return {
        ...state,
        addOk: action.payload,
      };
    case 'EDITAR_PRODUCTO':
      return {
        ...state,
        editOk: action.payload,
      };
    case 'ELIMINAR_PRODUCTO':
      return {
        ...state,
        deleteOk: action.payload,
      };
    case 'OBTENER_PRODUCTO':
      return {
        ...state,
        producto: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
