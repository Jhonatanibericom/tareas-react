import Rutas from './rutas';

import { Provider } from 'react-redux';
import { store } from './redux/store/store';

// import AuthState from './context/auth/AuthState';
// import ProductoState from './context/Productos/ProductoState';

function App() {
  return (
    // <AuthState>
    //   <ProductoState>
    <Provider store={store}>
      <Rutas />
    </Provider>
    //   </ProductoState>
    // </AuthState>
  );
}

export default App;
