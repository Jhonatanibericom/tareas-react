import { firebase } from '../../firebase/firebaseConfig';

export const startLoginEmailAndPassword = (email, password) => {
  return async (dispatch) => {
    try {
      const { user } = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      dispatch(login(user.uid, user.email));
    } catch (error) {
      console.log(error);
    }
  };
};

export const startLogout = () => {
  return async (dispatch) => {
    try {
      await firebase.auth().signOut();
      dispatch(logout());
    } catch (error) {
      console.log(error);
    }
  };
};

const logout = () => {
  return {
    type: 'LOGOUT',
  };
};

const login = (uid, email) => {
  return {
    type: 'LOGIN',
    payload: { uid, email },
  };
};
