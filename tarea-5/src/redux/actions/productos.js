import { db } from '../../firebase/firebaseConfig.js';

export const obtenerProductos = () => {
  return async (dispatch) => {
    try {
      const doc = await db.collection('productos').get();

      const productos = [];

      doc.forEach((item) => {
        productos.push({
          id: item.id,
          ...item.data(),
        });
      });

      dispatch({
        type: 'LLENAR_PRODUCTOS',
        payload: productos,
      });
    } catch (error) {
      console.log(error);
    }
  };
};

export const agregarProducto = (producto) => {
  return async (dispatch) => {
    try {
      await db.collection('productos').add(producto);
      dispatch({
        type: 'AGREGAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log('mira el error');
      console.log(error);
    }
  };
};

export const obtenerProducto = (id) => {
  return async (dispatch) => {
    try {
      const doc = await db.collection('productos').doc(id).get();
      let producto = {
        id: id,
        ...doc.data(),
      };

      console.log('los datos del producto');
      console.log(producto);

      dispatch({
        type: 'OBTENER_PRODUCTO',
        payload: producto,
      });
    } catch (error) {}
  };
};

export const editarProducto = (producto) => {
  return async (dispatch) => {
    try {
      const productoUpdate = { ...producto };

      delete productoUpdate.id;

      await db.collection('productos').doc(producto.id).update(productoUpdate);
      dispatch({
        type: 'EDITAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log('mira el error');
      console.log(error);
    }
  };
};

export const eliminarProducto = (id) => {
  return async (dispatch) => {
    try {
      await db.collection('productos').doc(id).delete();
      dispatch({
        type: 'ELIMINAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log('mira el error');
      console.log(error);
    }
  };
};
