import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyASgntcw6hGS2RZ4U_uMK5EwzEuG_W-sPU',
  authDomain: 'diplomado-front.firebaseapp.com',
  projectId: 'diplomado-front',
  storageBucket: 'diplomado-front.appspot.com',
  messagingSenderId: '895763455276',
  appId: ':895763455276:web:fb440fb96ad6063b5ea089',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore(); // axios

export { db, firebase };
