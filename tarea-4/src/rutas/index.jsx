import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

import Login from '../views/Login';
import Productos from '../views/productos';
import AgregarProducto from '../views/productos/Agregar';
import EditarProducto from '../views/productos/Editar';
import DetalleProducto from '../views/productos/Detalle';

import RutaPublica from './RutaPublica';
import RutaPrivada from './RutaPrivada';

import { firebase } from '../firebase/firebaseConfig';

const Rutas = () => {
  const [revisando, setRevisando] = useState(true);
  const [estaAutenticado, setEstaAutenticado] = useState(false);

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      console.log('el usuario');
      console.log(user);
      if (user?.uid) {
        setEstaAutenticado(true);
      } else {
        setEstaAutenticado(false);
      }
      setRevisando(false);
    });
  }, []);

  if (revisando) return <h1 className='text-center'> Cargando ...</h1>;

  return (
    <Router>
      <Switch>
        <RutaPublica
          exact
          estaAutenticado={estaAutenticado}
          path='/'
          component={Login}
        />
        <RutaPrivada
          exact
          estaAutenticado={estaAutenticado}
          path='/productos'
          component={Productos}
        />
        <RutaPrivada
          exact
          estaAutenticado={estaAutenticado}
          path='/producto/nuevo'
          component={AgregarProducto}
        />
        <RutaPrivada
          exact
          estaAutenticado={estaAutenticado}
          path='/producto/editar/:id'
          component={EditarProducto}
        />
        <RutaPrivada
          exact
          estaAutenticado={estaAutenticado}
          path='/producto/detalle/:id'
          component={DetalleProducto}
        />
      </Switch>
    </Router>
  );
};

export default Rutas;
