import Rutas from './rutas';

import AuthState from './context/auth/AuthState';
import ProductoState from './context/Productos/ProductoState';

function App() {
  return (
    <AuthState>
      <ProductoState>
        <Rutas />;
      </ProductoState>
    </AuthState>
  );
}

export default App;
