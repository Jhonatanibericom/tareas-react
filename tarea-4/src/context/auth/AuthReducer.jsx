const reducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        uid: action.payload.uid,
        email: action.payload.email,
      };
    case 'LOGOUT':
      return {
        uid: null,
        email: null,
      };

    default:
      return state;
  }
};

export default reducer;
