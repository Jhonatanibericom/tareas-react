import React, { useReducer } from 'react';

import { firebase } from '../../firebase/firebaseConfig';

import AuthContext from './AuthContext';
import AuthReducer from './AuthReducer';

const AuthState = (props) => {
  const initialState = {
    email: null,
    uid: null,
  };

  const startLoginEmailAndPassword = async (email, password) => {
    try {
      const { user } = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);

      //   dispatch({
      //     type: 'LOGIN',
      //     payload: { uid: user.uid, email: user.email },
      //   });
      dispatch(login(user.uid, user.email));
    } catch (error) {
      console.log(error);
    }
  };

  const startLogout = async () => {
    try {
      await firebase.auth().signOut();
      dispatch(logout());
    } catch (error) {
      console.log(error);
    }
  };

  const logout = () => {
    return {
      type: 'LOGOUT',
    };
  };

  const login = (uid, email) => {
    return {
      type: 'LOGIN',
      payload: { uid, email },
    };
  };

  const [state, dispatch] = useReducer(AuthReducer, initialState);

  return (
    <AuthContext.Provider
      value={{
        uid: state.uid,
        email: state.email,
        startLoginEmailAndPassword,
        startLogout,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
