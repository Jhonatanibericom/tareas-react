import React, { useReducer } from 'react';
import { db } from '../../firebase/firebaseConfig.js';
import ProductoContext from './ProductoContext';
import ProductoReducer from './ProductoReducer';

const ProductoState = (props) => {
  const initialState = {
    listaProductos: [],
    producto: {},
    addOk: false,
    editOk: false,
    deleteOk: false,
  };

  const obtenerProductos = async () => {
    try {
      const doc = await db.collection('productos').get();

      const productos = [];

      doc.forEach((item) => {
        productos.push({
          id: item.id,
          ...item.data(),
        });
      });

      dispatch({
        type: 'LLENAR_PRODUCTOS',
        payload: productos,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const agregarProducto = async (producto) => {
    try {
      await db.collection('productos').add(producto);
      dispatch({
        type: 'AGREGAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log('mira el error');
      console.log(error);
    }
  };

  const obtenerProducto = async (id) => {
    console.log('ejecutando');
    try {
      const doc = await db.collection('productos').doc(id).get();
      let producto = {
        id: id,
        ...doc.data(),
      };

      console.log('los datos del producto');
      console.log(producto);

      dispatch({
        type: 'OBTENER_PRODUCTO',
        payload: producto,
      });
    } catch (error) {}
  };

  const editarProducto = async (producto) => {
    try {
      const productoUpdate = { ...producto };

      delete productoUpdate.id;

      await db.collection('productos').doc(producto.id).update(productoUpdate);
      dispatch({
        type: 'EDITAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log('mira el error');
      console.log(error);
    }
  };

  const eliminarProducto = async (id) => {
    try {
      await db.collection('productos').doc(id).delete();
      dispatch({
        type: 'ELIMINAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log('mira el error');
      console.log(error);
    }
  };

  const [state, dispatch] = useReducer(ProductoReducer, initialState);
  return (
    <ProductoContext.Provider
      value={{
        listaProductos: state.listaProductos,
        addOk: state.addOk,
        editOk: state.editOk,
        deleteOk: state.deleteOk,
        producto: state.producto,
        obtenerProductos,
        agregarProducto,
        obtenerProducto,
        editarProducto,
        eliminarProducto,
      }}
    >
      {props.children}
    </ProductoContext.Provider>
  );
};

export default ProductoState;
