import React, { useContext } from 'react';
import { Link } from 'react-router-dom';

import ProductoContext from '../../context/Productos/ProductoContext';

const RowTabla = ({ plato, indice }) => {
  const productoContext = useContext(ProductoContext);
  const { eliminarProducto } = productoContext;
  const { id, nombre, categoria, precio } = plato;

  return (
    <tr>
      <th scope='row'>{indice + 1}</th>
      <td>{nombre}</td>
      <td>{categoria}</td>
      <td>S/ {precio}</td>
      <td className='d-flex justify-content-around'>
        <Link className='btn btn-warning' to={`/producto/editar/${id}`}>
          Editar
        </Link>
        <Link className='btn btn-warning' to={`/producto/detalle/${id}`}>
          Ver detalle
        </Link>
        <button className='btn btn-danger' onClick={() => eliminarProducto(id)}>
          Eliminar
        </button>
      </td>
    </tr>
  );
};

export default RowTabla;
