import React, { useContext, useEffect } from 'react';
import Formulario from '../../components/Formulario';

import ProductoContext from '../../context/Productos/ProductoContext';

const AgregarProducto = (props) => {
  const productoContext = useContext(ProductoContext);

  const { addOk, agregarProducto } = productoContext;

  useEffect(() => {
    if (addOk) {
      props.history.push('/productos');
    }
  }, [addOk]);

  const nuevoProducto = (producto) => {
    agregarProducto(producto);
  };

  return (
    <div>
      <h1>Agregar Producto</h1>
      <Formulario agregarProductoNuevo={nuevoProducto} />
    </div>
  );
};

export default AgregarProducto;
