import React, { useContext, useEffect } from 'react';
import ProductoContext from '../../context/Productos/ProductoContext';
import RowTabla from '../../components/RowTabla';

const DetalleProducto = (props) => {
    const productoContext = useContext(ProductoContext);

    const { editOk, producto, obtenerProducto, editarProducto } = productoContext;

    useEffect(() => {
        console.log('mira el id');
        console.log(props.match.params.id);
        obtenerProducto(props.match.params.id);
    }, []);

    const { id, nombre, categoria, precio, decripcion } = producto;

    return (
        <div>
            <h1>Detalle Producto</h1>
            <table className='table'>
                <thead className='thead-dark'>
                    <tr>
                        <th scope='col'>#</th>
                        <th scope='col'>Nombre</th>
                        <th scope='col'>Categoria</th>
                        <th scope='col'>Precio</th>
                        <th scope='col'>Descripción</th>
                    </tr>
                </thead>
                <tbody>
                <th scope='row'>{id}</th>
                <td>{nombre}</td>
                <td>{categoria}</td>
                <td>S/ {precio}</td>
                <td>{decripcion}</td>
                </tbody>
            </table>
        </div>
    );
};

export default DetalleProducto;