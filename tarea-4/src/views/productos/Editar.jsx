import React, { useContext, useEffect } from 'react';
import Formulario from '../../components/Formulario';

import ProductoContext from '../../context/Productos/ProductoContext';

const EditarProducto = (props) => {
  const productoContext = useContext(ProductoContext);

  const { editOk, producto, obtenerProducto, editarProducto } = productoContext;

  useEffect(() => {
    console.log('mira el id');
    console.log(props.match.params.id);
    obtenerProducto(props.match.params.id);
  }, []);

  useEffect(() => {
    if (editOk) {
      props.history.push('/productos');
    }
  }, [editOk]);

  const tieneProducto = Object.keys(producto).length;

  return (
    <div>
      <h1>Editar Producto</h1>
      {tieneProducto > 0 && (
        <Formulario
          producto={producto}
          editar={true}
          editarProducto={editarProducto}
        />
      )}
    </div>
  );
};

export default EditarProducto;
