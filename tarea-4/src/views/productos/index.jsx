import React, { useContext, useEffect } from 'react';
import Tabla from '../../components/Tabla';

import ProductoContext from '../../context/Productos/ProductoContext';

const Productos = () => {
  const productoContext = useContext(ProductoContext);

  const { deleteOk, listaProductos, obtenerProductos } = productoContext;

  useEffect(() => {
    obtenerProductos();
  }, []);

  useEffect(() => {
    if (deleteOk) {
      obtenerProductos();
    }
  }, [deleteOk]);

  return (
    <div>
      <div>
        <h1>Productos</h1>
        {listaProductos.length > 0 && <Tabla listaProductos={listaProductos} />}
      </div>
    </div>
  );
};

export default Productos;
