import React, { useContext, useState } from 'react';
import AuthContext from '../../context/auth/AuthContext';

const Login = () => {
  const authContext = useContext(AuthContext);
  const { startLoginEmailAndPassword } = authContext;

  const [formValues, setFormValues] = useState({
    email: 'eduardo@correo.com',
    password: '123456',
  });

  const handleChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    startLoginEmailAndPassword(formValues.email, formValues.password);
  };

  return (
    <div
      className='global-container mt-5'
      style={{ width: '20em', margin: '0 auto' }}
    >
      <div className='card login-form'>
        <div className='card-body'>
          <h3 className='card-title text-center'>Iniciar sesión</h3>
          <div className='card-text'>
            <form onSubmit={handleSubmit}>
              <div className='form-group'>
                <label htmlFor='exampleInputEmail1'>Correo</label>
                <input
                  onChange={handleChange}
                  value={formValues.email}
                  name='email'
                  type='email'
                  className='form-control form-control-sm'
                />
              </div>
              <div className='form-group'>
                <label htmlFor='exampleInputPassword1'>Contraseña</label>
                <input
                  type='password'
                  className='form-control form-control-sm'
                  onChange={handleChange}
                  value={formValues.password}
                  name='password'
                />
              </div>
              <div className='d-grid gap-2 col mx-auto mt-2'>
                <button type='submit' className='btn btn-primary'>
                  Iniciar
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
